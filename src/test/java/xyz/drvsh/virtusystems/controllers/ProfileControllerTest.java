package xyz.drvsh.virtusystems.controllers;

import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import xyz.drvsh.virtusystems.AbstractTest;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;
import xyz.drvsh.virtusystems.services.profile.pojo.ProfileList;

class ProfileControllerTest extends AbstractTest {
    static final String URI = "/profile";

    @Test
    void getBy() throws Exception {
        int i = 0;
        Profile profileIn = null;
        // пополним таблицу профилей, заодно проверим добавление
        while (i++ < 5) {
            String url = URI + "/add";
            profileIn = createProfileWithoutPassport();
            String inputJson = mapToJson(profileIn);
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(url)
                                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                    .content(inputJson)).andReturn();
            int status = mvcResult.getResponse().getStatus();
            assertEquals(200, status);
            String content = mvcResult.getResponse().getContentAsString();
            Profile profileOut = mapFromJson(content, Profile.class);

            assertTrue(isProfilesMatch(profileIn, profileOut) && profileOut.getId() != null);
        }

        // получение список профилей (в данном случае из одного элемента)
        String uri = URI + "/getBy";

        String inputJson = mapToJson(profileIn);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                                                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                .content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        ProfileList profileList = mapFromJson(content, ProfileList.class);
        assertTrue(isProfilesMatch(Objects.requireNonNull(profileIn), profileList.getProfileList().iterator().next()));
    }

    @Test
    void edit() throws Exception {
        // сначала добавим профиль и сразу проверим
        String url = URI + "/add";
        Profile profileAddIn = createProfileWithoutPassport();
        String inputAddJson = mapToJson(profileAddIn);
        MvcResult mvcResultAdd = mvc.perform(MockMvcRequestBuilders.post(url)
                                                                   .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                   .content(inputAddJson)).andReturn();
        assertEquals(200, mvcResultAdd.getResponse().getStatus());
        Profile profileAddOut = mapFromJson(mvcResultAdd.getResponse().getContentAsString(), Profile.class);

        assertTrue(isProfilesMatch(profileAddIn, profileAddOut) && profileAddOut.getId() != null);

        // отредактируем
        String uri = URI + "/edit";
        profileAddOut.setPassportSeries(getStringNum(4)) //
                     .setPassportNumber(getStringNum(6));
        String inputJson = mapToJson(profileAddOut);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                                                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                .content(inputJson)).andReturn();
        assertEquals(200, mvcResult.getResponse().getStatus());
        Profile profileOut = mapFromJson(mvcResult.getResponse().getContentAsString(), Profile.class);

        assertTrue(isProfilesMatch(profileAddOut, profileOut) && profileOut.getId() != null);
    }

    @SuppressWarnings("OverlyComplexBooleanExpression")
    private boolean isProfilesMatch(Profile profileIn, Profile profileOut) {
        return profileIn.getFirstName().equals(profileOut.getFirstName()) &&
               profileIn.getLastName().equals(profileOut.getLastName()) &&
               profileIn.getMiddleName().equals(profileOut.getMiddleName()) &&
               profileIn.getBirthDay().equals(profileOut.getBirthDay()) && //
               (profileIn.getPassportSeries() == null ||
                profileIn.getPassportSeries().equals(profileOut.getPassportSeries())) &&
               (profileIn.getPassportNumber() == null ||
                profileIn.getPassportNumber().equals(profileOut.getPassportNumber()));
    }
}
