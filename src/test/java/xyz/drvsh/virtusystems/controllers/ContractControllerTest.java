package xyz.drvsh.virtusystems.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import xyz.drvsh.virtusystems.AbstractTest;
import xyz.drvsh.virtusystems.db.beans.enums.ELibCountris;
import xyz.drvsh.virtusystems.services.contract.ContractService;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractRequest;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.Contract;
import xyz.drvsh.virtusystems.services.contract.pojo.ListContractsResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.RealtyAddress;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;

class ContractControllerTest extends AbstractTest {
    private static final String URI = "/contract";

    @Test
    void calculatePremium() throws Exception {
        String uri = URI + "/calculate";

        CalcContractRequest calcContractIn = createCalcContractRequest();
        String inputJson = super.mapToJson(calcContractIn);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                                                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        String dateCalc = LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        String premium = String.valueOf(ContractService.calcPremium(calcContractIn));
        boolean boo =
            content.contains("\"dateCalculation\":\"" + dateCalc + "\"") && content.contains("\"premium\":" + premium);
        assertTrue(content, boo);

    }

    @Test
    void saveContract() throws Exception {
        String uri = URI + "/save";

        /* Добавляем профиль */
        Profile profileOut;
        {
            Profile profileIn = createProfile();
            String inputJson = mapToJson(profileIn);
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(ProfileControllerTest.URI + "/add")
                                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                    .content(inputJson)).andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            profileOut = mapFromJson(content, Profile.class);
        }
        /* Добавляем калькуляцию */
        CalcContractResponse calcContract;
        {
            CalcContractRequest calcContractIn = createCalcContractRequest();
            String inputJson = super.mapToJson(calcContractIn);
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URI + "/calculate")
                                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                    .content(inputJson)).andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            calcContract = mapFromJson(content, CalcContractResponse.class);
        }

        // сохраняем договор
        Contract contractIn = new Contract();
        contractIn.setContractNumber(getStringNum(8));
        contractIn.setDate(LocalDate.now());
        contractIn.setProfile(profileOut);
        RealtyAddress realtyAddress = new RealtyAddress();
        realtyAddress.setCountry(ELibCountris.RUSSIA)
                     .setPostCode(getStringNum(6))
                     .setRegion(getString(7))
                     .setDistrict(getString(8))
                     .setCity(getString(8))
                     .setStreet(getString(10))
                     .setHouse(getStringNum(2))
                     .setHousing(getString(1))
                     .setBuilding(getString(1))
                     .setApartment(getStringNum(3));
        contractIn.setAddress(realtyAddress);
        contractIn.setCalcContract(calcContract);
        contractIn.setComment(getString(50));

        String inputJson = super.mapToJson(contractIn);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URI + "/save")
                                                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                .content(inputJson)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

    }

    @Test
    void getListContract() throws Exception {
        saveContract();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URI + "/list")).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        ListContractsResponse list = mapFromJson(content, ListContractsResponse.class);
        assertNotNull(list);
    }

    @Test
    void getContract() throws Exception {
        saveContract();
        // получить список и выбрать первый
        long id;
        {
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URI + "/list")).andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            int status = mvcResult.getResponse().getStatus();
            assertEquals(200, status);
            ListContractsResponse list = mapFromJson(content, ListContractsResponse.class);
            id = list.getListContracts().iterator().next().getId();
        }

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URI + "/get/" + id)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        Contract contract = mapFromJson(content, Contract.class);
        assertNotNull(contract);
    }

}
