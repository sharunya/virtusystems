package xyz.drvsh.virtusystems;

import java.text.MessageFormat;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import xyz.drvsh.virtusystems.db.beans.ContractJpa;
import xyz.drvsh.virtusystems.db.beans.ProfileJpa;
import xyz.drvsh.virtusystems.db.repositories.IContractRepository;
import xyz.drvsh.virtusystems.db.repositories.IProfileRepository;
import xyz.drvsh.virtusystems.services.profile.ProfileService;

@RunWith(SpringRunner.class)
public class VirtusystemsApplicationTests extends AbstractTest {

    @Autowired
    private IProfileRepository profileCrudRepository;

    @Autowired
    private IContractRepository contractRepository;

    @Before
    public void setUp() throws Exception {
        profileCrudRepository.deleteAll();
        contractRepository.deleteAll();
    }

    @Test
    @Transactional
    @Commit
    public void testAddProfileCrudRepository() {
        ProfileJpa profileJpa = ProfileService.getProfileJpaFromProfile(createProfile());

        System.out.println(MessageFormat.format("ProfileJpa = {0}", profileCrudRepository.save(profileJpa)));
    }

    @Test
    @Transactional
    public void testGetProfile() {
        ProfileJpa profileJpaIn = profileCrudRepository.save(ProfileService.getProfileJpaFromProfile(createProfile()));

        Optional<ProfileJpa> profiles = profileCrudRepository.findById(profileJpaIn.getId());
        ProfileJpa profileJpaOut = profiles.orElse(null);
        Assert.assertEquals("EQUALS PROFILE", profileJpaOut, profileJpaIn);
    }

    @Test
    @Transactional
    @Commit
    public void testAddContractCrudRepository() {

        ContractJpa contractJpa = createContract();
        Optional<ProfileJpa> profiles = profileCrudRepository.findById(0L);
        contractJpa.setProfileJpa(profiles.orElse(ProfileService.getProfileJpaFromProfile(createProfile())));

        System.out.println(MessageFormat.format("ContractJpa = {0}", contractRepository.save(contractJpa)));
    }

    @Test
    @Transactional
    public void testGetContractCrudRepository() {
        ContractJpa contractJpaIn = createContract();
        Optional<ProfileJpa> profiles = profileCrudRepository.findById(0L);
        contractJpaIn.setProfileJpa(profiles.orElse(ProfileService.getProfileJpaFromProfile(createProfile())));

        System.out.println(MessageFormat.format("save ContractJpa = {0}", contractRepository.save(contractJpaIn)));

        Optional<ContractJpa> optionalContract = contractRepository.findById(contractJpaIn.getId());
        System.out.println(MessageFormat.format("get ContractJpa = {0}", optionalContract));
        Assert.assertEquals("EQUALS CONTRACT", contractJpaIn, optionalContract.orElse(null));
    }

}
