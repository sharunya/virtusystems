package xyz.drvsh.virtusystems;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Random;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import xyz.drvsh.virtusystems.db.beans.CalcContractJpa;
import xyz.drvsh.virtusystems.db.beans.ContractJpa;
import xyz.drvsh.virtusystems.db.beans.RealtyAddressJpa;
import xyz.drvsh.virtusystems.db.beans.enums.ELibConstructionYears;
import xyz.drvsh.virtusystems.db.beans.enums.ELibCountris;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyAreas;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyTypes;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractRequest;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = VirtusystemsApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public abstract class AbstractTest {
    @Autowired
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void init() {
        Locale.setDefault(Locale.ENGLISH);
    }

    public CalcContractRequest createCalcContractRequest() {
        CalcContractRequest calcContract = new CalcContractRequest(LocalDate.now().minusMonths(1),
                                                                   LocalDate.now().plusMonths(5),
                                                                   ELibRealtyTypes.APARTMENT,
                                                                   ELibConstructionYears.OLD,
                                                                   ELibRealtyAreas.BIG,
                                                                   100000);
        return calcContract;
    }

    public CalcContractJpa createCalcContract() {
        CalcContractJpa calcContractJpa = new CalcContractJpa();
        calcContractJpa.setValidFrom(LocalDate.now().minusMonths(1))
                       .setValidUntil(LocalDate.now().plusMonths(5))
                       .setSumInsured(100000)
                       .setRealtyTypes(ELibRealtyTypes.APARTMENT)
                       .setConstructionYear(ELibConstructionYears.OLD)
                       .setRealtyArea(ELibRealtyAreas.BIG);
        return calcContractJpa;
    }

    @Before
    protected void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz
                               ) throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    protected Profile createProfile() {
        Profile profile = createProfileWithoutPassport();
        return profile.setPassportSeries(getStringNum(4)) //
                      .setPassportNumber(getStringNum(6));
    }

    protected Profile createProfileWithoutPassport() {
        Profile profile = new Profile(getString(8), getString(8), getString(8));
        profile.setBirthDay(generateDate());
        return profile;
    }

    ContractJpa createContract() {
        ContractJpa contractJpa = new ContractJpa();
        contractJpa.setContractNumber("001");
        contractJpa.setDate(LocalDate.now());
        CalcContractJpa calcContractJpa = createCalcContract();
        contractJpa.setCalcContractJpa(calcContractJpa);
        RealtyAddressJpa address = createRealtyAddress(contractJpa);
        contractJpa.setAddress(address);
        contractJpa.setComment(getString(20));
        return contractJpa;
    }

    private RealtyAddressJpa createRealtyAddress(ContractJpa contractJpa) {
        RealtyAddressJpa address = new RealtyAddressJpa();
        address.setCountry(ELibCountris.RUSSIA)
               .setPostCode("614000")
               .setRegion("Пермский край")
               .setDistrict("")
               .setCity("Пермь")
               .setStreet("улица")
               .setHouse("99")
               .setHousing("б")
               .setBuilding("1")
               .setApartment("15")
               .setOwner(contractJpa);
        return address;
    }

    protected String getStringNum(int length) {
        String chars = "1234567890";
        return generateString(chars, length);
    }

    protected String getString(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return generateString(chars, length);
    }

    private String generateString(String chars, int length) {
        StringBuilder builder = new StringBuilder();
        Random rnd = new Random();
        while (builder.length() < length) {
            int index = (int)(rnd.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        return builder.toString();
    }

    private LocalDate generateDate() {
        LocalDate start = LocalDate.of(1900, Month.JANUARY, 1);
        long days = ChronoUnit.DAYS.between(start, LocalDate.now().minusYears(16));
        return start.plusDays(new Random().nextInt((int)days + 1));
    }
}
