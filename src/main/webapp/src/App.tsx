import * as React from "react";
import "./css/App.scss";

import logo from "./logo.svg";
import AComponent from "./AComponent";
import {Button} from "react-toolbox/lib/button/Button";

class App extends AComponent {
    public render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.tsx</code> and save to reload.
                    <section style={{padding: 20}}>
                        <Button label="Primary Button" primary />
                    </section>
                </p>
                <a href={"/api/visits"}>qqqqqqqqqqq</a>
            </div>
        );
    }
}

export default App;
