import * as React from "react";

export interface IComponentProps /*extends React.HTMLAttributes<HTMLElement>*/ {
    children?: React.ReactNode | React.ReactNode[] | null | undefined;
    className?: string;
    style?: React.CSSProperties;
}

export interface IComponentStates {
    loading: boolean;
    // requestError?: IRequestExecutorResponse;
}

export default abstract class AComponent<P = {}, S = IComponentStates> extends React.Component<
    P & IComponentProps,
    S & IComponentStates
> {
    public state: S & IComponentStates;

    abstract render(): React.ReactNode;
}
