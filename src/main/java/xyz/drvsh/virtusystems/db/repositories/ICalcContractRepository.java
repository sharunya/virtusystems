package xyz.drvsh.virtusystems.db.repositories;

import org.springframework.stereotype.Repository;

import xyz.drvsh.virtusystems.db.beans.CalcContractJpa;

@Repository
public interface ICalcContractRepository extends ICustomRepository<CalcContractJpa, Long> {}
