package xyz.drvsh.virtusystems.db.repositories;

import org.springframework.stereotype.Repository;

import xyz.drvsh.virtusystems.db.beans.ContractJpa;

@Repository
public interface IContractRepository extends ICustomRepository<ContractJpa, Long> {}
