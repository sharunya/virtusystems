package xyz.drvsh.virtusystems.db.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import xyz.drvsh.virtusystems.db.beans.ProfileJpa;

@Repository
public interface IProfileRepository extends ICustomRepository<ProfileJpa, Long> {

    @Query("select p from ProfileJpa p where p.firstName like %?1% and p.lastName like %?2% and p.middleName like %?3%")
    List<ProfileJpa> findProfilesByFullName(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("middleName") String middleName);
}
