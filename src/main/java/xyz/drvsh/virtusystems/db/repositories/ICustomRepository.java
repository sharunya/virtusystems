package xyz.drvsh.virtusystems.db.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ICustomRepository<T, ID> extends CrudRepository<T, ID> {}
