package xyz.drvsh.virtusystems.db.beans;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import xyz.drvsh.virtusystems.db.beans.enums.ELibCountris;

@SuppressWarnings("ClassWithTooManyFields")
@Entity
@Table(name = "T_REALTY_ADDRESS")
public class RealtyAddressJpa {
    @Id
    @Column(name = "ADDRESS_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ADDRESS_SEQ")
    @SequenceGenerator(name = "ADDRESS_SEQ", sequenceName = "ADDRESS_SEQ", allocationSize = 1)
    private Long id;
    @Column(name = "COUNTRY")
    @Enumerated(EnumType.ORDINAL)
    private ELibCountris country;
    @Basic
    @Column(name = "POST_CODE")
    private String postCode;
    @Basic
    @Column(name = "REGION")
    private String region;
    /** район */
    @Basic
    @Column(name = "DISTRICT")
    private String district;
    @Basic
    @Column(name = "CITY")
    private String city;
    @Basic
    @Column(name = "STREET")
    private String street;
    @Basic
    @Column(name = "HOUSE")
    private String house;
    /** корпус */
    @Basic
    @Column(name = "HOUSING")
    private String housing;
    /** строение */
    @Basic
    @Column(name = "BUILDING")
    private String building;
    @Basic
    @Column(name = "APARTMENT")
    private String apartment;
    @OneToOne(optional = false, mappedBy = "address")
    private ContractJpa owner;

    public Long getId() {
        return id;
    }

    public RealtyAddressJpa setId(Long id) {
        this.id = id;
        return this;
    }

    public ELibCountris getCountry() {
        return country;
    }

    public RealtyAddressJpa setCountry(ELibCountris country) {
        this.country = country;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public RealtyAddressJpa setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public RealtyAddressJpa setRegion(String region) {
        this.region = region;
        return this;
    }

    public String getDistrict() {
        return district;
    }

    public RealtyAddressJpa setDistrict(String district) {
        this.district = district;
        return this;
    }

    public String getCity() {
        return city;
    }

    public RealtyAddressJpa setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public RealtyAddressJpa setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getHouse() {
        return house;
    }

    public RealtyAddressJpa setHouse(String house) {
        this.house = house;
        return this;
    }

    public String getHousing() {
        return housing;
    }

    public RealtyAddressJpa setHousing(String housing) {
        this.housing = housing;
        return this;
    }

    public String getBuilding() {
        return building;
    }

    public RealtyAddressJpa setBuilding(String building) {
        this.building = building;
        return this;
    }

    public String getApartment() {
        return apartment;
    }

    public RealtyAddressJpa setApartment(String apartment) {
        this.apartment = apartment;
        return this;
    }

    public ContractJpa getOwner() {
        return owner;
    }

    public RealtyAddressJpa setOwner(ContractJpa owner) {
        this.owner = owner;
        return this;
    }

    @SuppressWarnings("OverlyComplexBooleanExpression")
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RealtyAddressJpa)) {
            return false;
        }
        RealtyAddressJpa that = (RealtyAddressJpa)o;
        return getId().equals(that.getId()) && getCountry() == that.getCountry() && getPostCode().equals(that.getPostCode()) && getRegion().equals(that.getRegion()) &&
               getDistrict().equals(that.getDistrict()) && getCity().equals(that.getCity()) && getStreet().equals(that.getStreet()) && getHouse().equals(that.getHouse()) &&
               getHousing().equals(that.getHousing()) && getBuilding().equals(that.getBuilding()) && getApartment().equals(that.getApartment()) && getOwner().equals(that.getOwner());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCountry(), getPostCode(), getRegion(), getDistrict(), getCity(), getStreet(), getHouse(), getHousing(), getBuilding(), getApartment(), getOwner());
    }

    @Override
    public String toString() {
        return "RealtyAddressJpa{" + "id=" + id + ", country=" + country + ", postCode='" + postCode + '\'' + ", region='" + region + '\'' + ", district='" + district + '\'' + ", city='" + city + '\'' +
               ", street='" + street + '\'' + ", house='" + house + '\'' + ", housing='" + housing + '\'' + ", building='" + building + '\'' + ", apartment='" + apartment + '\'' + ", owner=" + owner.getId() +
               '}';
    }
}
