package xyz.drvsh.virtusystems.db.beans;

import java.time.LocalDate;
import java.util.StringJoiner;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig.LocalDateDeserializer;
import xyz.drvsh.virtusystems.config.AppConfig.LocalDateSerializer;
import xyz.drvsh.virtusystems.db.beans.enums.ELibConstructionYears;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyAreas;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyTypes;

@Entity
@Table(name = "T_CALC_CONTRACT")
public class CalcContractJpa {
    @Id
    @Column(name = "CALC_CONTRACT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTRACT_SEQ")
    @SequenceGenerator(name = "CONTRACT_SEQ", sequenceName = "CONTRACT_SEQ", allocationSize = 1)
    private Long id;

    @Basic
    @Column(name = "VALID_FROM")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate validFrom;

    @Basic
    @Column(name = "VALID_UNTIL")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate validUntil;

    /** Коэф.ТН – Коэффициент за тип недвижимости в процентах */
    @Column(name = "REALTY_TYPES")
    @Enumerated(EnumType.ORDINAL)
    private ELibRealtyTypes realtyTypes;

    /** Коэф.ГП – Коэффициент за год постройки */
    @Column(name = "CONSTRUCTION_YEAR")
    @Enumerated(EnumType.ORDINAL)
    private ELibConstructionYears constructionYear;

    /** Коэф.Пл – Коэффициент за площадь */
    @Column(name = "REALTY_AREA")
    @Enumerated(EnumType.ORDINAL)
    private ELibRealtyAreas realtyArea;

    /** страховая сумма в копейках */
    @Basic
    @Column(name = "SUM_INSURED")
    private int sumInsured;

    /** страховая премия в копейках */
    @Basic
    @Column(name = "PREMIUM")
    private Long premium;

    /** дата расчета стрх премии */
    @Basic
    @Column(name = "DATE_CALCULATION")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateCalculation;

    public Long getId() {
        return id;
    }

    public CalcContractJpa setId(Long id) {
        this.id = id;
        return this;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public CalcContractJpa setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public CalcContractJpa setValidUntil(LocalDate validUntil) {
        this.validUntil = validUntil;
        return this;
    }

    public ELibRealtyTypes getRealtyTypes() {
        return realtyTypes;
    }

    public CalcContractJpa setRealtyTypes(ELibRealtyTypes realtyTypes) {
        this.realtyTypes = realtyTypes;
        return this;
    }

    public ELibConstructionYears getConstructionYear() {
        return constructionYear;
    }

    public CalcContractJpa setConstructionYear(ELibConstructionYears constructionYear) {
        this.constructionYear = constructionYear;
        return this;
    }

    public ELibRealtyAreas getRealtyArea() {
        return realtyArea;
    }

    public CalcContractJpa setRealtyArea(ELibRealtyAreas realtyArea) {
        this.realtyArea = realtyArea;
        return this;
    }

    public int getSumInsured() {
        return sumInsured;
    }

    public CalcContractJpa setSumInsured(int sumInsured) {
        this.sumInsured = sumInsured;
        return this;
    }

    public Long getPremium() {
        return premium;
    }

    public CalcContractJpa setPremium(Long premium) {
        this.premium = premium;
        return this;
    }

    public LocalDate getDateCalculation() {
        return dateCalculation;
    }

    public CalcContractJpa setDateCalculation(LocalDate dateCalculation) {
        this.dateCalculation = dateCalculation;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CalcContractJpa)) {
            return false;
        }

        CalcContractJpa that = (CalcContractJpa)o;

        if (getSumInsured() != that.getSumInsured()) {
            return false;
        }
        if (getPremium() != that.getPremium()) {
            return false;
        }
        if (!getId().equals(that.getId())) {
            return false;
        }
        if (!getValidFrom().equals(that.getValidFrom())) {
            return false;
        }
        if (!getValidUntil().equals(that.getValidUntil())) {
            return false;
        }
        if (getRealtyTypes() != that.getRealtyTypes()) {
            return false;
        }
        if (getConstructionYear() != that.getConstructionYear()) {
            return false;
        }
        if (getRealtyArea() != that.getRealtyArea()) {
            return false;
        }
        return getDateCalculation().equals(that.getDateCalculation());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getValidFrom().hashCode();
        result = 31 * result + getValidUntil().hashCode();
        result = 31 * result + getRealtyTypes().hashCode();
        result = 31 * result + getConstructionYear().hashCode();
        result = 31 * result + getRealtyArea().hashCode();
        result = 31 * result + getSumInsured();
        result = 31 * result + (int)(getPremium() ^ (getPremium() >>> 32));
        result = 31 * result + getDateCalculation().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CalcContractJpa.class.getSimpleName() + "[", "]").add("id=" + id)
                                                                                       .add("validFrom=" + validFrom)
                                                                                       .add("validUntil=" + validUntil)
                                                                                       .add("realtyTypes=" + realtyTypes)
                                                                                       .add("constructionYear=" + constructionYear)
                                                                                       .add("realtyArea=" + realtyArea)
                                                                                       .add("sumInsured=" + sumInsured)
                                                                                       .add("premium=" + premium)
                                                                                       .add("dateCalculation=" + dateCalculation)
                                                                                       .toString();
    }
}

