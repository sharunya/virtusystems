package xyz.drvsh.virtusystems.db.beans.enums;

/** Коэф.ГП – Коэффициент за год постройки */
public enum ELibConstructionYears {
    OLD(0, "Меньше 2000", 1.3),
    MIDDLE(1, "2000-2014", 1.6),
    YOUNG(2, "2015", 2.0);

    private final int id;
    private final String desc;
    private final double coefficient;

    ELibConstructionYears(int id, String desc, double coefficient) {
        this.id = id;
        this.desc = desc;
        this.coefficient = coefficient;
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public double getCoefficient() {
        return coefficient;
    }
}
