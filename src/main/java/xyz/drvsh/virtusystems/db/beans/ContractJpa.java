package xyz.drvsh.virtusystems.db.beans;

import java.time.LocalDate;
import java.util.StringJoiner;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;

@Entity
@Table(name = "T_CONTRACT")
public class ContractJpa {

    @Id
    @Column(name = "CONTRACT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTRACT_SEQ")
    @SequenceGenerator(name = "CONTRACT_SEQ", sequenceName = "CONTRACT_SEQ", allocationSize = 1)
    private Long id;

    @Basic
    @Column(name = "CONTRACT_NUMBER")
    private String contractNumber;

    @Basic
    @Column(name = "DATE")
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate date;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ProfileJpa.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "P_ID")
    private ProfileJpa profileJpa;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID")
    private RealtyAddressJpa address;

    @OneToOne(optional = false, targetEntity = CalcContractJpa.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "CALC_CONTRACT_ID")
    private CalcContractJpa calcContractJpa;

    @Basic
    @Column(name = "COMMENT")
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ProfileJpa getProfileJpa() {
        return profileJpa;

    }

    public void setProfileJpa(ProfileJpa profileJpa) {
        this.profileJpa = profileJpa;
    }

    public RealtyAddressJpa getAddress() {
        return address;
    }

    public void setAddress(RealtyAddressJpa address) {
        this.address = address;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public CalcContractJpa getCalcContractJpa() {
        return calcContractJpa;
    }

    public ContractJpa setCalcContractJpa(CalcContractJpa calcContractJpa) {
        this.calcContractJpa = calcContractJpa;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContractJpa)) {
            return false;
        }

        ContractJpa contractJpa = (ContractJpa)o;

        if (!getId().equals(contractJpa.getId())) {
            return false;
        }
        if (!getContractNumber().equals(contractJpa.getContractNumber())) {
            return false;
        }
        if (!getDate().equals(contractJpa.getDate())) {
            return false;
        }
        if (!getProfileJpa().equals(contractJpa.getProfileJpa())) {
            return false;
        }
        if (!getAddress().equals(contractJpa.getAddress())) {
            return false;
        }
        if (!getCalcContractJpa().equals(contractJpa.getCalcContractJpa())) {
            return false;
        }
        return getComment().equals(contractJpa.getComment());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getContractNumber().hashCode();
        result = 31 * result + getDate().hashCode();
        result = 31 * result + getProfileJpa().hashCode();
        result = 31 * result + getAddress().hashCode();
        result = 31 * result + getCalcContractJpa().hashCode();
        result = 31 * result + getComment().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ContractJpa.class.getSimpleName() + "[", "]").add("id=" + id)
                                                                                   .add("contractNumber='" + contractNumber + "'")
                                                                                   .add("date=" + date)
                                                                                   .add("profileJpa=" + profileJpa)
                                                                                   .add("address=" + address)
                                                                                   .add("calcContractJpa=" + calcContractJpa)
                                                                                   .add("comment='" + comment + "'")
                                                                                   .toString();
    }
}
