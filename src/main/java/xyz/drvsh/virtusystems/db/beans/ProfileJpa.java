package xyz.drvsh.virtusystems.db.beans;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;

@Entity
@Table(name = "T_PROFILE")
public class ProfileJpa {
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthDay;
    private String passportSeries;
    private String passportNumber;

    @Id
    @Column(name = "P_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PROFILE_SEQ")
    @SequenceGenerator(name = "PROFILE_SEQ", sequenceName = "PROFILE_SEQ", allocationSize = 1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "MIDDLE_NAME")
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Basic
    @Column(name = "BIRTH_DAY")
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    @Basic
    @Column(name = "PASSPORT_SERIES")
    public String getPassportSeries() {
        return passportSeries;
    }

    public ProfileJpa setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
        return this;
    }

    @Basic
    @Column(name = "PASSPORT_NUMBER")
    public String getPassportNumber() {
        return passportNumber;
    }

    public ProfileJpa setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfileJpa)) {
            return false;
        }
        ProfileJpa profileJpa = (ProfileJpa)o;
        return getId().equals(profileJpa.getId()) && getLastName().equals(profileJpa.getLastName()) && getFirstName().equals(profileJpa.getFirstName()) && getMiddleName().equals(profileJpa.getMiddleName()) &&
               getBirthDay().equals(profileJpa.getBirthDay()) && getPassportSeries().equals(profileJpa.getPassportSeries()) && getPassportNumber().equals(profileJpa.getPassportNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLastName(), getFirstName(), getMiddleName(), getBirthDay(), getPassportSeries(), getPassportNumber());
    }

    @Override
    public String toString() {
        return "ProfileJpa{" + "id=" + id + ", lastName='" + lastName + '\'' + ", firstName='" + firstName + '\'' + ", middleName='" + middleName + '\'' + ", birthDay=" + birthDay +
               ", passportSeries='" + passportSeries + '\'' + ", passportNumber='" + passportNumber + "'}";
    }
}
