package xyz.drvsh.virtusystems.db.beans.enums;

/** Коэф.Пл – Коэффициент за площадь */
public enum ELibRealtyAreas {
    SMALL(0, "Менее 50 кв.м.", 1.2),
    MIDDLE(1, "50-100 кв.м.", 1.5),
    BIG(2, "Более 100 кв.м.", 2.0);

    private final int id;
    private final String desc;
    private final double coefficient;

    ELibRealtyAreas(int id, String desc, double coefficient) {
        this.id = id;
        this.desc = desc;
        this.coefficient = coefficient;
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public double getCoefficient() {
        return coefficient;
    }
}
