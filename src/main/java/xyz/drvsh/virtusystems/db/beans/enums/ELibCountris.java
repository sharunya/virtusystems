package xyz.drvsh.virtusystems.db.beans.enums;

/** Список стран */
public enum ELibCountris {
    RUSSIA(0, "Россия"),
    UKRAINE(1, "Украина"),
    BELARUS(2, "Белорусия");

    private final int id;
    private final String name;

    ELibCountris(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
