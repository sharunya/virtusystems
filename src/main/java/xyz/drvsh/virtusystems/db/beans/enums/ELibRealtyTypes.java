package xyz.drvsh.virtusystems.db.beans.enums;

/** Коэф.ТН – Коэффициент за тип недвижимости в процентах */
public enum ELibRealtyTypes {
    APARTMENT(0, "Квартира", 1.7),
    HOUSE(1, "Дом", 1.5),
    ROOM(2, "Комната", 1.3);

    private final int id;
    private final String desc;
    private final double coefficient;

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public double getCoefficient() {
        return coefficient;
    }

    ELibRealtyTypes(int id, String desc, double coefficient) {
        this.id = id;
        this.desc = desc;
        this.coefficient = coefficient;
    }
}
