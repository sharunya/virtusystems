package xyz.drvsh.virtusystems.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DefaultResponse {
    @JsonProperty
    boolean isError = false;
}
