package xyz.drvsh.virtusystems.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import xyz.drvsh.virtusystems.services.profile.IProfileService;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;
import xyz.drvsh.virtusystems.services.profile.pojo.ProfileList;

@SuppressWarnings("ClassHasNoToStringMethod")
@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final IProfileService profileService;

    public ProfileController(IProfileService profileService) {this.profileService = profileService;}

    @ResponseBody
    @PostMapping("/getBy")
    public ProfileList getBy(@RequestBody Profile profile) {
        return profileService.findProfileByNames(profile);
    }

    @ResponseBody
    @PostMapping("/add")
    public Profile add(@RequestBody Profile profile) {
        return profileService.addProfile(profile);
    }

    @ResponseBody
    @PostMapping("/edit")
    public Profile edit(@RequestBody Profile profile) {
        return profileService.editProfile(profile);
    }

}

