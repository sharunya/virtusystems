package xyz.drvsh.virtusystems.controllers;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import xyz.drvsh.virtusystems.db.beans.enums.ELibConstructionYears;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyAreas;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyTypes;
import xyz.drvsh.virtusystems.services.contract.IContractService;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractRequest;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.Contract;
import xyz.drvsh.virtusystems.services.contract.pojo.InfoResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.ListContractsResponse;

@RestController
@RequestMapping("/contract")
public class ContractController {

    private final IContractService contractService;

    public ContractController(IContractService contractService) {this.contractService = contractService;}

    @ResponseBody
    @PostMapping("/calculate")
    public ResponseEntity<CalcContractResponse> calculatePremium(@NotNull @RequestBody CalcContractRequest calcContract) {
        return new ResponseEntity<>(contractService.calcContract(calcContract), HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/list")
    public ResponseEntity<ListContractsResponse> getListContract() {
        return new ResponseEntity<>(contractService.getListContract(), HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/save")
    public ResponseEntity<DefaultResponse> saveContract(@NotNull @RequestBody Contract contract) {
        return new ResponseEntity<>(contractService.saveContract(contract), HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/get/{id}")
    public ResponseEntity<Contract> getContract(@NotNull @PathVariable Long id) {
        Contract contract = contractService.getContract(id);
        return new ResponseEntity<>(contract, HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/info")
    public ResponseEntity<InfoResponse> getInfo() {
        InfoResponse info = new InfoResponse();
        info.realtyTypes = Stream.of(ELibRealtyTypes.values())
                                 .map(eLibRealtyTypes -> new InfoResponse.LibRealtyTypes(eLibRealtyTypes.getId(),
                                                                                         eLibRealtyTypes.name(),
                                                                                         eLibRealtyTypes.getDesc(),
                                                                                         eLibRealtyTypes.getCoefficient()))
                                 .collect(Collectors.toList());
        info.constructionYears = Stream.of(ELibConstructionYears.values())
                                       .map(constrYears -> new InfoResponse.LibConstructionYears(constrYears.getId(),
                                                                                                 constrYears.name(),
                                                                                                 constrYears.getDesc(),
                                                                                                 constrYears.getCoefficient()))
                                       .collect(Collectors.toList());
        info.realtyAreas = Stream.of(ELibRealtyAreas.values())
                                 .map(realtyAreas -> new InfoResponse.LibRealtyAreas(realtyAreas.getId(),
                                                                                     realtyAreas.name(),
                                                                                     realtyAreas.getDesc(),
                                                                                     realtyAreas.getCoefficient()))
                                 .collect(Collectors.toList());
        return new ResponseEntity<>(info, HttpStatus.OK);
    }

}
