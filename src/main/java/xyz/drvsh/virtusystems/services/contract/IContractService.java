package xyz.drvsh.virtusystems.services.contract;


import xyz.drvsh.virtusystems.controllers.DefaultResponse;
import xyz.drvsh.virtusystems.exeptions.ValidationException;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractRequest;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.Contract;
import xyz.drvsh.virtusystems.services.contract.pojo.ListContractsResponse;

public interface IContractService {
    /** Расчет премии */
    CalcContractResponse calcContract(CalcContractRequest calcContract);

    /** Сохранение договора */
    DefaultResponse saveContract(Contract contract);

    /** Список договоров */
    ListContractsResponse getListContract();

    /** Открытие договора */
    Contract getContract(Long id) throws ValidationException;

    /** Изменение данных договора */
    Contract editContract(Long id);
}
