package xyz.drvsh.virtusystems.services.contract;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import xyz.drvsh.virtusystems.config.AppConfig;
import xyz.drvsh.virtusystems.controllers.DefaultResponse;
import xyz.drvsh.virtusystems.db.beans.CalcContractJpa;
import xyz.drvsh.virtusystems.db.beans.ContractJpa;
import xyz.drvsh.virtusystems.db.beans.ProfileJpa;
import xyz.drvsh.virtusystems.db.beans.RealtyAddressJpa;
import xyz.drvsh.virtusystems.db.repositories.ICalcContractRepository;
import xyz.drvsh.virtusystems.db.repositories.IContractRepository;
import xyz.drvsh.virtusystems.db.repositories.IProfileRepository;
import xyz.drvsh.virtusystems.exeptions.ValidationException;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractRequest;
import xyz.drvsh.virtusystems.services.contract.pojo.CalcContractResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.Contract;
import xyz.drvsh.virtusystems.services.contract.pojo.ListContractsResponse;
import xyz.drvsh.virtusystems.services.contract.pojo.RealtyAddress;
import xyz.drvsh.virtusystems.services.contract.pojo.SimpleContractResponse;
import xyz.drvsh.virtusystems.services.profile.ProfileService;

@Service
public class ContractService implements IContractService {

    private final ICalcContractRepository calcContractRepository;
    private final IContractRepository contractRepository;
    private final IProfileRepository profileRepository;

    public ContractService(ICalcContractRepository calcContractRepository,
                           IContractRepository contractRepository,
                           IProfileRepository profileRepository) {
        this.calcContractRepository = calcContractRepository;
        this.contractRepository = contractRepository;
        this.profileRepository = profileRepository;
    }

    /**
     * Расчет премии
     */
    @Override
    public CalcContractResponse calcContract(CalcContractRequest calcContract) {
        long premium = calcPremium(calcContract);
        LocalDate dateCalculation = LocalDate.now();

        CalcContractJpa calcContractJpa = convertCalcContract(calcContract, premium, dateCalculation);

        CalcContractJpa saved = calcContractRepository.save(calcContractJpa);
        return new CalcContractResponse(saved.getId(), saved.getPremium(), saved.getDateCalculation());
    }

    @Override
    public DefaultResponse saveContract(Contract contract) {

        ContractJpa contractJpa = new ContractJpa();
        contractJpa.setContractNumber(contract.getContractNumber());
        contractJpa.setDate(contract.getDate());
        Optional<ProfileJpa> profileJpa = profileRepository.findById(contract.getProfile().getId());
        contractJpa.setProfileJpa(profileJpa.orElse(null));
        RealtyAddress address = contract.getAddress();
        RealtyAddressJpa realtyAddressJpa = new RealtyAddressJpa();
        realtyAddressJpa.setCountry(address.getCountry())
                        .setPostCode(address.getPostCode())
                        .setRegion(address.getRegion())
                        .setDistrict(address.getDistrict())
                        .setCity(address.getCity())
                        .setStreet(address.getStreet())
                        .setHouse(address.getHouse())
                        .setHousing(address.getHousing())
                        .setBuilding(address.getBuilding())
                        .setApartment(address.getApartment())
                        .setOwner(contractJpa);
        contractJpa.setAddress(realtyAddressJpa);
        Optional<CalcContractJpa> calcContractJpa = calcContractRepository.findById(contract.getCalcContract().getId());
        contractJpa.setCalcContractJpa(calcContractJpa.orElse(null));
        contractJpa.setComment(contract.getComment());
        contractRepository.save(contractJpa);
        return new DefaultResponse();
    }

    @Override
    public ListContractsResponse getListContract() {
        Iterable<ContractJpa> contracts = contractRepository.findAll();
        return new ListContractsResponse().setListContracts(StreamSupport.stream(contracts.spliterator(), false)
                                                                         .map(this::getSimpleContractResponseFromContractJpa)
                                                                         .collect(Collectors.toList()));
    }

    @Override
    public Contract getContract(Long id) throws ValidationException {
        Optional<ContractJpa> optionalContractJpa = contractRepository.findById(id);
        if (!optionalContractJpa.isPresent()) {
            throw new ValidationException("Договор не найден");
        }
        ContractJpa contractJpa = optionalContractJpa.orElse(new ContractJpa());

        Contract contract = new Contract();
        contract.setContractNumber(contractJpa.getContractNumber());
        contract.setDate(contractJpa.getDate());
        contract.setProfile(ProfileService.getProfileFromProfileJpa(contractJpa.getProfileJpa()));
        RealtyAddressJpa addressJpa = contractJpa.getAddress();
        RealtyAddress realtyAddress = new RealtyAddress();
        realtyAddress.setCountry(addressJpa.getCountry())
                     .setPostCode(addressJpa.getPostCode())
                     .setRegion(addressJpa.getRegion())
                     .setDistrict(addressJpa.getDistrict())
                     .setCity(addressJpa.getCity())
                     .setStreet(addressJpa.getStreet())
                     .setHouse(addressJpa.getHouse())
                     .setHousing(addressJpa.getHousing())
                     .setBuilding(addressJpa.getBuilding())
                     .setApartment(addressJpa.getApartment());
        contract.setAddress(realtyAddress);
        CalcContractResponse calcContract = new CalcContractResponse(contractJpa.getCalcContractJpa().getId(),
                                                                     contractJpa.getCalcContractJpa().getPremium(),
                                                                     contractJpa.getCalcContractJpa()
                                                                                .getDateCalculation());
        contract.setCalcContract(calcContract);
        contract.setComment(contractJpa.getComment());

        return contract;
    }

    @Override
    public Contract editContract(Long id) {
        return null;
    }

    // Вспомогательные ф-ции

    SimpleContractResponse getSimpleContractResponseFromContractJpa(ContractJpa contractJpa) {
        return new SimpleContractResponse(contractJpa.getId(),
                                          contractJpa.getContractNumber(),
                                          contractJpa.getDate(),
                                          MessageFormat.format("{0} {1} {2}",
                                                               contractJpa.getProfileJpa().getLastName(),
                                                               contractJpa.getProfileJpa().getFirstName(),
                                                               contractJpa.getProfileJpa().getMiddleName()),
                                          contractJpa.getCalcContractJpa().getPremium(),
                                          MessageFormat.format("{0}-{1}",
                                                               contractJpa.getCalcContractJpa()
                                                                          .getValidFrom()
                                                                          .format(AppConfig.DATE_TIME_FORMATTER),
                                                               contractJpa.getCalcContractJpa()
                                                                          .getValidUntil()
                                                                          .format(AppConfig.DATE_TIME_FORMATTER)));
    }

    public static CalcContractJpa convertCalcContract(CalcContractRequest request,
                                                      long premium,
                                                      LocalDate dateCalculation) {
        CalcContractJpa calcContractJpa = new CalcContractJpa();
        calcContractJpa.setValidFrom(request.getValidFrom())
                       .setValidUntil(request.getValidUntil())
                       .setRealtyTypes(request.getRealtyTypes())
                       .setConstructionYear(request.getConstructionYear())
                       .setRealtyArea(request.getRealtyArea())
                       .setSumInsured(request.getSumInsured())
                       .setPremium(premium)
                       .setDateCalculation(dateCalculation);
        return calcContractJpa;
    }

    public static long calcPremium(CalcContractRequest request) {
        long countDay = ChronoUnit.DAYS.between(request.getValidFrom(), request.getValidUntil());
        return Math.round((request.getSumInsured() / (double)countDay) * (request.getRealtyTypes().getCoefficient()) *
                          (request.getConstructionYear().getCoefficient()) *
                          (request.getRealtyArea().getCoefficient()));
    }
}
