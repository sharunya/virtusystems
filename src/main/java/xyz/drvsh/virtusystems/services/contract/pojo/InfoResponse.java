package xyz.drvsh.virtusystems.services.contract.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import xyz.drvsh.virtusystems.controllers.DefaultResponse;

public class InfoResponse extends DefaultResponse {
    @JsonProperty
    public List<LibRealtyTypes> realtyTypes;
    @JsonProperty
    public List<LibConstructionYears> constructionYears;
    @JsonProperty
    public List<LibRealtyAreas> realtyAreas;

    public static class LibRealtyTypes {
        @JsonProperty
        private final int id;
        @JsonProperty
        private final String name;
        @JsonProperty
        private final String desc;
        @JsonProperty
        private final double coefficient;

        public LibRealtyTypes(int id, String name, String desc, double coefficient) {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.coefficient = coefficient;
        }
    }

    public static class LibConstructionYears {
        @JsonProperty
        private final int id;
        @JsonProperty
        private final String name;
        @JsonProperty
        private final String desc;
        @JsonProperty
        private final double coefficient;

        public LibConstructionYears(int id, String name, String desc, double coefficient) {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.coefficient = coefficient;
        }
    }

    public static class LibRealtyAreas {
        @JsonProperty
        private final int id;
        @JsonProperty
        private final String name;
        @JsonProperty
        private final String desc;
        @JsonProperty
        private final double coefficient;

        public LibRealtyAreas(int id, String name, String desc, double coefficient) {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.coefficient = coefficient;
        }
    }
}
