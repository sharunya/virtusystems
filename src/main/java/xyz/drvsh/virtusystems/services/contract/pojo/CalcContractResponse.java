package xyz.drvsh.virtusystems.services.contract.pojo;

import java.time.LocalDate;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;

public class CalcContractResponse {
    @JsonProperty
    private Long id;
    /** страховая премия в копейках */
    @JsonProperty
    private Long premium;
    /** дата расчета стрх премии */
    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate dateCalculation;

    public CalcContractResponse() {
        //
    }

    public CalcContractResponse(Long id, Long premium, LocalDate dateCalculation) {
        this.id = id;
        this.premium = premium;
        this.dateCalculation = dateCalculation;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CalcContractResponse.class.getSimpleName() + "[", "]").add("id=" + id)
                                                                                            .add("premium=" + premium)
                                                                                            .add("dateCalculation=" +
                                                                                                 dateCalculation)
                                                                                            .toString();
    }
}
