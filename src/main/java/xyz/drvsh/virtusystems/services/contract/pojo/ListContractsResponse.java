package xyz.drvsh.virtusystems.services.contract.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListContractsResponse {
    @JsonProperty
    List<SimpleContractResponse> listContracts;

    public ListContractsResponse setListContracts(List<SimpleContractResponse> listContracts) {
        this.listContracts = listContracts;
        return this;
    }

    public List<SimpleContractResponse> getListContracts() {
        return listContracts;
    }
}
