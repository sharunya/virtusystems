package xyz.drvsh.virtusystems.services.contract.pojo;

import java.time.LocalDate;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;
import xyz.drvsh.virtusystems.db.beans.enums.ELibConstructionYears;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyAreas;
import xyz.drvsh.virtusystems.db.beans.enums.ELibRealtyTypes;

public class CalcContractRequest {
    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate validFrom;
    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate validUntil;
    /** Коэф.ТН – Коэффициент за тип недвижимости в процентах */
    @JsonProperty
    private ELibRealtyTypes realtyTypes;
    @JsonProperty
    private ELibConstructionYears constructionYear;
    /** Коэф.Пл – Коэффициент за площадь */
    @JsonProperty
    private ELibRealtyAreas realtyArea;
    /** страховая сумма в копейках */
    @JsonProperty
    private int sumInsured;

    public CalcContractRequest() {
        // для десериализации
    }

    public CalcContractRequest(LocalDate validFrom, LocalDate validUntil, ELibRealtyTypes realtyTypes, ELibConstructionYears constructionYear, ELibRealtyAreas realtyArea, int sumInsured) {
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.realtyTypes = realtyTypes;
        this.constructionYear = constructionYear;
        this.realtyArea = realtyArea;
        this.sumInsured = sumInsured;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public ELibRealtyTypes getRealtyTypes() {
        return realtyTypes;
    }

    public ELibConstructionYears getConstructionYear() {
        return constructionYear;
    }

    public ELibRealtyAreas getRealtyArea() {
        return realtyArea;
    }

    public int getSumInsured() {
        return sumInsured;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CalcContractRequest.class.getSimpleName() + "[", "]").add("validFrom=" + validFrom)
                                                                                           .add("validUntil=" + validUntil)
                                                                                           .add("realtyTypes=" + realtyTypes)
                                                                                           .add("constructionYear=" + constructionYear)
                                                                                           .add("realtyArea=" + realtyArea)
                                                                                           .add("sumInsured=" + sumInsured)
                                                                                           .toString();
    }
}
