package xyz.drvsh.virtusystems.services.contract.pojo;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;

public class Contract {
    @JsonProperty
    private Long id;
    @JsonProperty
    private @NotBlank String contractNumber;
    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate date;
    @JsonProperty
    private Profile profile;
    @JsonProperty
    private RealtyAddress address;
    @JsonProperty
    private CalcContractResponse calcContract;
    @JsonProperty
    private String comment;

    public Long getId() {
        return id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public Profile getProfile() {
        return profile;
    }

    public RealtyAddress getAddress() {
        return address;
    }

    public CalcContractResponse getCalcContract() {
        return calcContract;
    }

    public String getComment() {
        return comment;
    }

    public Contract setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
        return this;
    }

    public Contract setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public Contract setProfile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public Contract setAddress(RealtyAddress address) {
        this.address = address;
        return this;
    }

    public Contract setCalcContract(CalcContractResponse calcContract) {
        this.calcContract = calcContract;
        return this;
    }

    public Contract setComment(String comment) {
        this.comment = comment;
        return this;
    }
}
