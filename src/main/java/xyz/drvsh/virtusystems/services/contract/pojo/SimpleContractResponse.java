package xyz.drvsh.virtusystems.services.contract.pojo;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;

public class SimpleContractResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String contractNumber;
    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    private LocalDate date;
    @JsonProperty
    private String fullName;
    @JsonProperty
    private Long premia;
    @JsonProperty
    private String validityPeriod;

    public SimpleContractResponse() {
        //
    }

    public SimpleContractResponse(Long id,
                                  String contractNumber,
                                  LocalDate date,
                                  String fullName,
                                  Long premia,
                                  String validityPeriod) {
        this.id = id;
        this.contractNumber = contractNumber;
        this.date = date;
        this.fullName = fullName;
        this.premia = premia;
        this.validityPeriod = validityPeriod;
    }

    public Long getId() {
        return id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getFullName() {
        return fullName;
    }

    public Long getPremia() {
        return premia;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    @Override
    public String toString() {
        return "SimpleContractResponse{" + "id=" + id + ", contractNumber='" + contractNumber + '\'' + ", date=" +
               date + ", fullName='" + fullName + '\'' + ", premia=" + premia + ", validityPeriod='" + validityPeriod +
               '\'' + '}';
    }
}
