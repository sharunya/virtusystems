package xyz.drvsh.virtusystems.services.contract.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import xyz.drvsh.virtusystems.db.beans.enums.ELibCountris;

public class RealtyAddress {
    @JsonProperty
    private ELibCountris country;
    @JsonProperty
    private String postCode;
    @JsonProperty
    private String region;
    /** район */
    @JsonProperty
    private String district;
    @JsonProperty
    private String city;
    @JsonProperty
    private String street;
    @JsonProperty
    private String house;
    /** корпус */
    @JsonProperty
    private String housing;
    /** строение */
    @JsonProperty
    private String building;
    @JsonProperty
    private String apartment;

    public ELibCountris getCountry() {
        return country;
    }

    public RealtyAddress setCountry(ELibCountris country) {
        this.country = country;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public RealtyAddress setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public RealtyAddress setRegion(String region) {
        this.region = region;
        return this;
    }

    public String getDistrict() {
        return district;
    }

    public RealtyAddress setDistrict(String district) {
        this.district = district;
        return this;
    }

    public String getCity() {
        return city;
    }

    public RealtyAddress setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public RealtyAddress setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getHouse() {
        return house;
    }

    public RealtyAddress setHouse(String house) {
        this.house = house;
        return this;
    }

    public String getHousing() {
        return housing;
    }

    public RealtyAddress setHousing(String housing) {
        this.housing = housing;
        return this;
    }

    public String getBuilding() {
        return building;
    }

    public RealtyAddress setBuilding(String building) {
        this.building = building;
        return this;
    }

    public String getApartment() {
        return apartment;
    }

    public RealtyAddress setApartment(String apartment) {
        this.apartment = apartment;
        return this;
    }
}
