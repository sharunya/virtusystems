package xyz.drvsh.virtusystems.services.profile.pojo;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import xyz.drvsh.virtusystems.config.AppConfig;

public class Profile {
    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @JsonProperty
    private @NotBlank String lastName;

    @JsonProperty
    private @NotBlank String firstName;

    @JsonProperty
    private @NotBlank String middleName;

    @JsonProperty
    @JsonSerialize(using = AppConfig.LocalDateSerializer.class)
    @JsonDeserialize(using = AppConfig.LocalDateDeserializer.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate birthDay;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String passportSeries;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String passportNumber;

    public Profile() {
        //
    }

    public Profile(Long id, String lastName, String firstName, String middleName, LocalDate birthDay, String passportSeries, String passportNumber) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
        this.passportSeries = passportSeries;
        this.passportNumber = passportNumber;
    }

    public Profile(String lastName, String firstName, String middleName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public Profile setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public Profile setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
        return this;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public Profile setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
        return this;
    }

    public Long getId() {
        return id;
    }
}
