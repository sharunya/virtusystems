package xyz.drvsh.virtusystems.services.profile;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import xyz.drvsh.virtusystems.db.beans.ProfileJpa;
import xyz.drvsh.virtusystems.db.repositories.IProfileRepository;
import xyz.drvsh.virtusystems.services.profile.pojo.Profile;
import xyz.drvsh.virtusystems.services.profile.pojo.ProfileList;

@Service
public class ProfileService implements IProfileService {
    private final IProfileRepository profileRepository;

    public ProfileService(IProfileRepository profileRepository) {this.profileRepository = profileRepository;}

    @Override
    public ProfileList findProfileByNames(Profile profile) {
        List<ProfileJpa> profileJpas = profileRepository.findProfilesByFullName(profile.getFirstName(),
                                                                                profile.getLastName(),
                                                                                profile.getMiddleName());
        ProfileList profileList = new ProfileList();
        profileList.setProfileList(profileJpas.stream()
                                              .map(ProfileService::getProfileFromProfileJpa)
                                              .collect(Collectors.toList()));
        return profileList;
    }

    @Override
    public Profile addProfile(Profile profile) {
        ProfileJpa profileJpa = getProfileJpaFromProfile(profile);
        return getProfileFromProfileJpa(profileRepository.save(profileJpa));
    }

    @Override
    public Profile editProfile(Profile profile) {
        ProfileJpa profileJpa = getProfileJpaFromProfile(profile);
        return getProfileFromProfileJpa(profileRepository.save(profileJpa));
    }

    public static ProfileJpa getProfileJpaFromProfile(Profile profile) {
        ProfileJpa profileJpa = new ProfileJpa();
        profileJpa.setId(profile.getId());
        profileJpa.setFirstName(profile.getFirstName());
        profileJpa.setLastName(profile.getLastName());
        profileJpa.setMiddleName(profile.getMiddleName());
        profileJpa.setBirthDay(profile.getBirthDay());
        profileJpa.setPassportSeries(profile.getPassportSeries());
        profileJpa.setPassportNumber(profile.getPassportNumber());
        return profileJpa;
    }

    public static Profile getProfileFromProfileJpa(ProfileJpa profileJpa) {
        return new Profile(profileJpa.getId(),
                           profileJpa.getLastName(),
                           profileJpa.getFirstName(),
                           profileJpa.getMiddleName(),
                           profileJpa.getBirthDay(),
                           profileJpa.getPassportSeries(),
                           profileJpa.getPassportNumber());
    }

}
