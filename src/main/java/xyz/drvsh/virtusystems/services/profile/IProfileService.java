package xyz.drvsh.virtusystems.services.profile;

import xyz.drvsh.virtusystems.services.profile.pojo.Profile;
import xyz.drvsh.virtusystems.services.profile.pojo.ProfileList;

public interface IProfileService {
    ProfileList findProfileByNames(Profile profile);

    Profile addProfile(Profile profile);

    Profile editProfile(Profile profile);

}
