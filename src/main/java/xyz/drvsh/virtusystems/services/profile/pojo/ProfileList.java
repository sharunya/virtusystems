package xyz.drvsh.virtusystems.services.profile.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfileList {
    @JsonProperty
    private List<Profile> profileList;

    public ProfileList setProfileList(List<Profile> profileList) {
        this.profileList = profileList;
        return this;
    }

    public List<Profile> getProfileList() {
        return profileList;
    }
}
